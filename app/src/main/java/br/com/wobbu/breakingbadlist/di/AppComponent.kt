package br.com.wobbu.breakingbadlist.di

import br.com.wobbu.breakingbadlist.view.main.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = [AppModule::class, UtilsModule::class])
@Singleton
interface AppComponent {
    fun doInjection(mainActivity: MainActivity)
}
