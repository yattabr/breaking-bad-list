package br.com.wobbu.breakingbadlist.model

import java.io.Serializable

class Characters : Serializable {
    var name: String = ""
    var nickname: String = ""
    var portrayed: String = ""
    var img: String = ""
    var status: String = ""
    var occupation: ArrayList<String> = arrayListOf()
    var appearance: ArrayList<String> = arrayListOf()
}