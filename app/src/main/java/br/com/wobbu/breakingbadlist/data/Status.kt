package br.com.wobbu.breakingbadlist.data

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}