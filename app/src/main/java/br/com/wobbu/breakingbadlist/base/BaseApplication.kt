package br.com.wobbu.breakingbadlist.base

import android.app.Application
import android.content.Context
import br.com.wobbu.breakingbadlist.di.UtilsModule
import br.com.wobbu.breakingbadlist.di.AppComponent
import br.com.wobbu.breakingbadlist.di.AppModule
import br.com.wobbu.breakingbadlist.di.DaggerAppComponent

open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    open fun getAppComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(AppModule(this)).utilsModule(UtilsModule())
            .build()
    }

    protected override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
    }
}