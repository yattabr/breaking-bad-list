package br.com.wobbu.breakingbadlist.view.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.wobbu.breakingbadlist.data.ApiResponse
import br.com.wobbu.breakingbadlist.data.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(val repository: Repository) : ViewModel() {
    val fetchCharactersObserver = MutableLiveData<ApiResponse>()
    private val disposables = CompositeDisposable()

    fun fetchCharacters() {
        disposables.add(repository.fetchCharacters()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { fetchCharactersObserver.setValue(ApiResponse.loading()) }
            .subscribe(
                { result -> fetchCharactersObserver.setValue(ApiResponse.success(result)) },
                { throwable -> fetchCharactersObserver.setValue(ApiResponse.error(throwable)) }
            ))
    }
}