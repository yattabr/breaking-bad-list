package br.com.wobbu.breakingbadlist.view.characterDetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.wobbu.breakingbadlist.R
import br.com.wobbu.breakingbadlist.model.Characters
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_character_detail.*

class CharacterDetailActivity : AppCompatActivity() {

    private lateinit var character: Characters

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_detail)

        character = intent.getSerializableExtra("character") as Characters

        initUI()
    }

    private fun initUI() {
        txt_character_name.text = character.name
        txt_occupation.text = "Occupation: ${character.occupation}"
        txt_status.text = "Status: ${character.status}"
        txt_nickname.text = "Nickname: ${character.nickname}"
        txt_season.text = "Seasons: ${character.appearance}"
        Glide.with(this).load(character.img).into(img_character)
    }
}
