package br.com.wobbu.breakingbadlist.data

import br.com.wobbu.breakingbadlist.model.Characters
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiCallInterface {

    @GET(Urls.FETCH_CHARACTERS)
    fun fetchCharacters(): Observable<ArrayList<Characters>>
}