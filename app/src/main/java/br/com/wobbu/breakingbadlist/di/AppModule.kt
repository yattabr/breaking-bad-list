package br.com.wobbu.breakingbadlist.di

import android.content.Context
import br.com.wobbu.breakingbadlist.view.main.CharactersAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule(private val context: Context) {

    @Provides
    @Singleton
    internal open fun provideContext(): Context {
        return context
    }

    @Provides
    internal open fun characterAdapter(): CharactersAdapter {
        return CharactersAdapter(context)
    }
}