package br.com.wobbu.breakingbadlist.data

class Urls {
    companion object {

        const val BASE_URL = "https://breakingbadapi.com/api/"
        const val FETCH_CHARACTERS = "characters"
    }
}