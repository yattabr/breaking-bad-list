package br.com.wobbu.breakingbadlist.data

import br.com.wobbu.breakingbadlist.model.Characters
import io.reactivex.Observable


class Repository(private val apiCallInterface: ApiCallInterface) {
    fun fetchCharacters(): Observable<ArrayList<Characters>> {
        return apiCallInterface.fetchCharacters()
    }
}