package br.com.wobbu.breakingbadlist.view.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.wobbu.breakingbadlist.R
import br.com.wobbu.breakingbadlist.base.BaseApplication
import br.com.wobbu.breakingbadlist.base.ViewModelFactory
import br.com.wobbu.breakingbadlist.data.ApiResponse
import br.com.wobbu.breakingbadlist.data.Status
import br.com.wobbu.breakingbadlist.model.Characters
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var charactersAdapter: CharactersAdapter
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as BaseApplication).getAppComponent().doInjection(this)
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        initObserver()
        initUI()
        mainViewModel.fetchCharacters()
    }

    private fun initUI() {
        edit_search.addTextChangedListener(filterWatcher)
    }

    private fun initObserver() {
        mainViewModel.fetchCharactersObserver.observe(this, Observer {
            when (it) {
                is ApiResponse -> charactersResponse(it)
            }
        })
    }

    private fun charactersResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.LOADING -> loading.visibility = View.VISIBLE
            Status.SUCCESS -> {
                loading.visibility = View.GONE
                mountRecyclerView(apiResponse.data as ArrayList<Characters>)
            }
            Status.ERROR -> {
                loading.visibility = View.GONE
                Log.i("API_RESPONSE_ERROR", apiResponse.error.toString())
            }
        }
    }

    private fun mountRecyclerView(items: ArrayList<Characters>) {
        charactersAdapter.loadCharacteres(items)
        recycler_view.adapter = charactersAdapter
    }

    private val filterWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            charactersAdapter.filter.filter(s)
        }
    }
}
