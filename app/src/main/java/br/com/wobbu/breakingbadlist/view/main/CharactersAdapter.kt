package br.com.wobbu.breakingbadlist.view.main

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import br.com.wobbu.breakingbadlist.R
import br.com.wobbu.breakingbadlist.model.Characters
import br.com.wobbu.breakingbadlist.view.characterDetail.CharacterDetailActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_character.view.*
import java.util.*

class CharactersAdapter(var context: Context) :
    RecyclerView.Adapter<CharactersAdapter.CustomViewHolder>(), Filterable {

    private var filteredList: ArrayList<Characters> = arrayListOf()
    private var charactersList: ArrayList<Characters> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_character, parent, false)
        return CustomViewHolder(context, view)
    }

    fun loadCharacteres(items: ArrayList<Characters>) {
        filteredList = items
        charactersList = items
    }

    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(filteredList[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                filteredList = if (charString.isEmpty()) {
                    charactersList
                } else {
                    val list = charactersList.filter { item ->
                        item.name.toLowerCase(Locale.getDefault())
                            .contains(charString.toLowerCase(Locale.getDefault()))
                    }
                    list as ArrayList<Characters>
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredList = results!!.values as ArrayList<Characters>
                notifyDataSetChanged()
            }

        }
    }

    class CustomViewHolder(private val context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(item: Characters) {
            itemView.txt_name.text = item.name
            itemView.txt_actor.text = "Actor: ${item.portrayed}"
            Glide.with(context).load(item.img).into(itemView.img_character)

            itemView.setOnClickListener {
                val intent = Intent(context, CharacterDetailActivity::class.java)
                intent.putExtra("character", item)
                intent.flags = FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            }
        }
    }
}