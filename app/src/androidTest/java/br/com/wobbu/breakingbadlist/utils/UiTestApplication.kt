package br.com.wobbu.breakingbadlist.utils

import br.com.wobbu.breakingbadlist.base.BaseApplication
import br.com.wobbu.breakingbadlist.di.AppComponent
import br.com.wobbu.breakingbadlist.di.DaggerAppComponent
import br.com.wobbu.breakingbadlist.di.MockAppModule
import br.com.wobbu.breakingbadlist.di.MockUtilsModule

class UiTestApplication : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun getAppComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(MockAppModule(this))
            .utilsModule(MockUtilsModule())
            .build()
    }

}