package br.com.wobbu.breakingbadlist.utils

import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

object MockServerDispatcher {
    /**
     * Return ok response from mock server
     */
    class RequestDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            when (request.path) {
                "/characters" -> {
                    return MockResponse().setResponseCode(200).setBody(
                        TestUtils().getFileString(
                            "mock/characters.json"
                        )
                    )
                }
            }
            return MockResponse().setResponseCode(404)
        }
    }

    /**
     * Return error response from mock server
     */
    class ErrorDispatcher : Dispatcher() {

        override fun dispatch(request: RecordedRequest): MockResponse {

            return MockResponse().setResponseCode(400)

        }
    }
}